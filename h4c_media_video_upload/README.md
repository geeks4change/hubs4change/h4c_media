# H4C Media Video Upload

## Configuration
- Install module
- Add upload button to ckeditor toolbar config of a text format

## Hide download button
- Currently the file_video template is overriden in enzian, see https://gitlab.com/geeks4change/hubs4change/enzian/-/blob/3.0.x/src/components/elements/file-video.html.twig
